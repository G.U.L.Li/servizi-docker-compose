# Traefik Dashboard
![Traefik Dashboard](https://gitlab.com/G.U.L.Li/servizi-docker-compose/-/raw/master/.immagini/traefik_dashboard.png)

### HTTP Routers
![Traefik HTTP Routers](https://gitlab.com/G.U.L.Li/servizi-docker-compose/-/raw/master/.immagini/traefik_http-routers.png)

### HTTP Services
![Traefik HTTP Services](https://gitlab.com/G.U.L.Li/servizi-docker-compose/-/raw/master/.immagini/traefik_http-services.png)

### HTTP Middlewares
![Traefik HTTP Middlewares](https://gitlab.com/G.U.L.Li/servizi-docker-compose/-/raw/master/.immagini/traefik_http-middlewares.png)

