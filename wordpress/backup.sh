#!/bin/sh

cd /srv/wordpress/

DATE=`date +"%Y-%m-%d"`
TAR_FILENAME="filesystem_${DATE}.sql"
SQL_FILENAME="database_${DATE}.sql"

tar -cpzf backups/$TAR_FILENAME wp-fs
docker-compose exec -T mariadb sh -c 'exec mysqldump "$MYSQL_DATABASE" -u"$MYSQL_USER" -p"$MYSQL_PASSWORD"' > backups/$SQL_FILENAME
chmod -R o= backups

