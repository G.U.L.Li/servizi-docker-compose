#!/bin/sh

cd /srv/testing-db/

DATE=`date +"%Y-%m-%d"`
FILENAME="database_${DATE}.sql"

docker-compose exec -T postgres sh -c 'pg_dump -U gulli-thorww2 -d thor_wwii' > database_backup/$FILENAME

