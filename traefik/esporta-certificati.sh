#!/bin/sh

cd /srv/traefik/

ACME=`cat acme.json`
CERTIFICATES=`echo $ACME | jq '.myresolver.Certificates[]'`
DOMAIN_CERTIFICATE=`echo $CERTIFICATES | jq 'select( .domain.main == "linux.livorno.it" )'`

KEY_B64=`echo $DOMAIN_CERTIFICATE | jq -r '.key'`
CERT_B64=`echo $DOMAIN_CERTIFICATE | jq -r '.certificate'`

echo $KEY_B64 | base64 --decode > certificates/privkey.pem
echo $CERT_B64 | base64 --decode > certificates/fullchain.pem

