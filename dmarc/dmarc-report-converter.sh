#!/bin/sh

set -e
DATE=`date +"%Y-%m-%d"`

cd $(dirname "$0")
# Elimina mail più vecchie di 30 giorni
#cp -a /var/mail/dmarc-reports backups/mail_${DATE}
#mutt -f /var/mail/dmarc-reports -e "push D~d>30d<enter>qy<enter>"
rm -r html/reports*
./dmarc-report-converter -config config.yaml
./dmarc-report-converter -config config.daily.yaml
